package com.fabcoders.addressView.retrofit

import com.fabcoders.addressView.model.login.loginModel
import com.fabcoders.addressView.model.login.loginRequest
import com.fabcoders.goancart.model.getAddress.getAddressModel
import retrofit2.Response
import retrofit2.http.*


interface APIServices {

    @GET("account/refreshToken")
    suspend fun refreshToken(@Query ("Customer_ID") cust_id: String, @Header("Authorization")token:String): Response<loginModel>

    @POST("account/login")
    suspend fun getLogin(@Body params : loginRequest): Response<loginModel>

    @GET("account/GetAddress")
    suspend fun getAddress(@Header("Authorization")  token:String): Response<getAddressModel>

}