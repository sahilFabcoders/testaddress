package com.fabcoders.addressView.retrofit


import android.content.Context
import com.franmontiel.persistentcookiejar.PersistentCookieJar
import com.franmontiel.persistentcookiejar.cache.SetCookieCache
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor
import okhttp3.CookieJar
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import okhttp3.logging.HttpLoggingInterceptor

object APIClient {

    const val BASE_URL = "http://3.135.185.114/goancart-admin-api/index.php/api/customer/"
    var retrofit: Retrofit? = null


    fun makeRetrofitService(context: Context): APIServices {

        if (retrofit==null){
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(makeOkHttpClient(context))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        return retrofit!!.create(APIServices::class.java)
    }

    private fun makeOkHttpClient(context: Context): OkHttpClient {
        var cookieJar: CookieJar = CookieJar.NO_COOKIES

        try {
            cookieJar = PersistentCookieJar(SetCookieCache(), SharedPrefsCookiePersistor(context))
        } catch (e: Exception) {
        }
        var okBuild = OkHttpClient.Builder()
            .addInterceptor(makeLoggingInterceptor())
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .writeTimeout(120, TimeUnit.SECONDS)

        if(cookieJar!=null){
            okBuild = okBuild. cookieJar(cookieJar)
        }
        return  okBuild.build()
    }


    private fun makeLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

}