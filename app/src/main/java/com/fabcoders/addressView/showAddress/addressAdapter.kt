package com.fabcoders.addressView.showAddress

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.addressView.R
import com.fabcoders.goancart.model.getAddress.getAddressModelItem
import kotlinx.android.synthetic.main.item_address_view.view.*

class addressAdapter(val partItemList: List<getAddressModelItem>, val listener: ItemClickListener, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        // Inflate XML. Last parameter: don't immediately attach new view to the parent view group
        val view = inflater.inflate(R.layout.item_address_view, parent, false)
        return PartViewHolder(view)
    }

    override fun getItemCount(): Int {
        return partItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val part = partItemList[position]
        holder.itemView.tvType.text = part.Address_Name
        holder.itemView.tvAddressMain.text = part.Address_Line_1+"\n"+ part.State+" "+ part.Pincode


        mClickListener = listener
        holder.itemView.btnDelete.setOnClickListener { view ->
            mClickListener?.clickToIntent(position)
        }
    }


    class PartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvType: TextView
        var tvAddressMain: TextView
        var btnDelete: Button


        init {
            tvType = itemView.findViewById(R.id.tvType)
            tvAddressMain = itemView.findViewById(R.id.tvAddressMain)
            btnDelete = itemView.findViewById(R.id.btnDelete)

        }

    }


    companion object {
        var mClickListener: ItemClickListener? = null
    }

    interface ItemClickListener
    {
        fun clickToIntent(position: Int)
    }

}