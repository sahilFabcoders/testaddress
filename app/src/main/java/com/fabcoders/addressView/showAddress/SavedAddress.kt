package com.fabcoders.addressView.showAddress

import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fabcoders.addressView.R
import com.fabcoders.addressView.getStringSharedPref
import com.fabcoders.addressView.model.address.addUpdateAddressRequest
import com.fabcoders.addressView.model.login.loginError
import com.fabcoders.addressView.namesVariable
import com.fabcoders.addressView.retrofit.APIClient
import com.fabcoders.addressView.toast
import com.fabcoders.goancart.model.getAddress.getAddressModelItem
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class SavedAddress : AppCompatActivity() {

    lateinit var rvAddress: RecyclerView
    lateinit var addressAdapters: addressAdapter
    lateinit var categoryItems: ArrayList<getAddressModelItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_saved_address)
        rvAddress=findViewById(R.id.rvAddress)
        categoryItems=ArrayList()
        toast("loading...")
        setupToolbar()
        initRecycler()
        loadAddress()

    }

    private fun loadAddress() {
        CoroutineScope(Dispatchers.IO).launch {
            val service = APIClient.makeRetrofitService(applicationContext)
            val response = service.getAddress(getStringSharedPref(namesVariable.ACCESSTOKEN,this@SavedAddress))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            categoryItems.clear()
                            val result = response.body()
                            for (x in 0 until  result!!.size){
                                categoryItems.add(result[x])
                            }
                            addressAdapters= addressAdapter(categoryItems, object : addressAdapter.ItemClickListener {
                                override fun clickToIntent(position: Int) {
                                    toast("Welcome!!")
                                }
                            },this@SavedAddress)
                            rvAddress.adapter = addressAdapters
                            addressAdapters.notifyDataSetChanged()
                        }
                    } else {
                        val gson = Gson()
                        val modelResult: loginError = gson.fromJson(response.errorBody()!!.charStream(), loginError::class.java)
                        toast(modelResult.Msg)
                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    private fun initRecycler() {
        rvAddress.layoutManager = LinearLayoutManager(this@SavedAddress)
        rvAddress.setHasFixedSize(true)
        addressAdapters= addressAdapter(categoryItems, object : addressAdapter.ItemClickListener {
            override fun clickToIntent(position: Int) {
                toast("Welcome!!")
            }
        },this@SavedAddress)
        rvAddress.adapter = addressAdapters
        addressAdapters.notifyDataSetChanged()
    }



    private fun setupToolbar() {
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        val toolbarText = findViewById(R.id.title) as TextView
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbarText.text="Saved Addresses"

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }
}
