package com.fabcoders.addressView

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Handler
import android.os.StrictMode
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*


fun Context?.toast(text: CharSequence) = Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
fun logd(text: String, tag: String = "UTIL") = Log.d(tag, text)

fun decodeString(data:String): String {
    val resultData: ByteArray = Base64.decode(data, Base64.DEFAULT)
    val text = String(resultData, Charsets.UTF_8)
    return text
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.show(time: Long = 4000) {
    visible()
    Handler().postDelayed({ gone() }, time)
}


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
fun String.toDate(): Date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(this)

fun Date.readableFull(): String = SimpleDateFormat("dd-MMM-yyyy hh:mm a", Locale.ENGLISH).format(this)

fun Any.log(tag: String = "tag") = Log.d(tag, this.toString())
fun Any.loge(tag: String = "tag") = Log.e(tag, this.toString())

fun Int.toDp(ctx: Context): Int {
    val density = ctx.resources
            .displayMetrics
            .density
    return Math.round(this * density)
}

private fun hasNetworkAvailable(context: Context): Boolean {
    val service = Context.CONNECTIVITY_SERVICE
    val manager = context.getSystemService(service) as ConnectivityManager?
    val network = manager?.activeNetworkInfo
    Log.d("", "hasNetworkAvailable: ${(network != null)}")
    return (network != null)
}

fun hasInternetConnected(context: Context): Boolean {
    val REACHABILITY_SERVER = "https://www.google.com"
    // Disable the `NetworkOnMainThreadException` and make sure it is just logged.
    StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build())
    if (hasNetworkAvailable(context)) {
        try {
            val connection = URL(REACHABILITY_SERVER).openConnection() as HttpURLConnection
            connection.setRequestProperty("User-Agent", "Test")
            connection.setRequestProperty("Connection", "close")
            connection.connectTimeout = 1000
            connection.connect()
            return (connection.responseCode == 200)
        } catch (e: IOException) {
            Log.e("", "Error checking internet connection", e)
        }
    } else {
        Log.w("", "No network available!")
    }
    Log.d("", "hasInternetConnected: false")
    return false
}


fun getStringSharedPref(key:String,context: Context): String {
    var sharedPreferences:SharedPreferences
    sharedPreferences = context.getSharedPreferences(context.getPackageName(), 0)
    return sharedPreferences.getString(key, "").toString()
}

fun setStringSharedPref(key:String,value_s:String,context: Context){
    var sharedPreferences:SharedPreferences
    sharedPreferences = context.getSharedPreferences(context.getPackageName(), 0)
    val editor = sharedPreferences.edit()
    editor.putString(key,value_s)
    editor.apply()
}


