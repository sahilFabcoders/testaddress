package com.fabcoders.addressView.model.login

data class loginModel(
    val Msg: String,
    val access_token: String,
    val expires_in: String,
    val token_type: String,
    val Type: String,
    val user: User
)