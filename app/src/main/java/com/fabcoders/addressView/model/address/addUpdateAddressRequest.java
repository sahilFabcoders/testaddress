package com.fabcoders.addressView.model.address;

public class addUpdateAddressRequest {
    public String Address_Line_1;
    public String Landmark;
    public String City;
    public String State;
    public String Pincode;
    public String Address_Name;
    public String lat;
    public String lon;
    public String Customer_Address_ID;
}
