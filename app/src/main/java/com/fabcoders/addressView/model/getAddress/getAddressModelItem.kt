package com.fabcoders.goancart.model.getAddress

data class getAddressModelItem(
    val Address_Line_1: String,
    val Address_Name: String,
    val City: String,
    val Customer_Address_ID: String,
    val Customer_ID: String,
    val IsDeleted: String,
    val IsSelected: String,
    val Landmark: String,
    val Pincode: String,
    val State: String,
    val createdAt: String,
    val lat: String,
    val lon: String,
    val updatedAt: String
)