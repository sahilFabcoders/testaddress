package com.fabcoders.addressView.model.login

data class loginError(
    val Code: Int,
    val Msg: String,
    val Type: String
)