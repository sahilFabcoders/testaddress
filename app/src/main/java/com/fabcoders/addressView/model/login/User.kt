package com.fabcoders.addressView.model.login

import java.math.BigInteger

data class User(
    val Customer_ID: String,
    val Customer_Name: String,
    val Customer_Points: BigInteger,
    val Device: Any,
    val Email: String,
    val IsDeleted: Boolean,
    val Phone_Number: String,
    val Referral_code: String,
    val Referral_code_used: Any,
    val createdAt: String,
    val updatedAt: String
)