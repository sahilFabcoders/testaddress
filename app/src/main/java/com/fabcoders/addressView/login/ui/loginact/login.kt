package com.fabcoders.addressView.login.ui.loginact

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.fabcoders.addressView.*
import com.fabcoders.addressView.showAddress.SavedAddress
import com.fabcoders.addressView.model.login.loginError
import com.fabcoders.addressView.model.login.loginModel
import com.fabcoders.addressView.model.login.loginRequest
import com.fabcoders.addressView.retrofit.APIClient
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.HttpException


class login : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val login = findViewById<Button>(R.id.login)


        login.setOnClickListener {
                login("7709974199","12345678")
        }

    }


    fun login(username: String, password: String){
/*//        using Okhttp code from postman*/
//        val client = OkHttpClient().newBuilder().build()
////        val mediaType = MediaType.parse("application/json")
//        val body = MultipartBody.Builder().setType(MultipartBody.FORM)
//            .addFormDataPart("Phone_Number", username)
//            .addFormDataPart("Password", password)
//            .build()
//        val request = Request.Builder()
//            .url("http://3.135.185.114/goancart-admin-api/index.php/api/customer/account/login")
//            .method("POST", body)
//            .addHeader("Content-Type", "application/json")
//            .build()
//        val response = client.newCall(request).execute()
//        if (response.isSuccessful){
//            var res=response.body!!.string()
//            val gson = Gson()
//            val modelResult = gson.fromJson(res, loginModel::class.java)
//
//            if (modelResult!!.Type != null&& (modelResult == null || res.isEmpty())) {
//                toast(modelResult!!.Msg)
//            } else if (!modelResult!!.user.IsDeleted) {
//                setStringSharedPref(namesVariable.EMAIL,modelResult!!.user.Email.toString(), applicationContext)
//                setStringSharedPref(namesVariable.MOBILE,modelResult!!.user.Phone_Number.toString(), applicationContext)
//                setStringSharedPref(namesVariable.PASSWORD,password, applicationContext)
//                setStringSharedPref(namesVariable.FULLNAME,modelResult!!.user.Customer_Name.toString(), applicationContext)
//                setStringSharedPref(namesVariable.CUSTOMERPOINTS,modelResult!!.user.Customer_Points.toString(), applicationContext)
//                setStringSharedPref(namesVariable.REFERAL,modelResult!!.user.Referral_code.toString(), applicationContext)
//                setStringSharedPref(namesVariable.USERID,modelResult!!.user.Customer_ID.toString(), applicationContext)
//                setStringSharedPref(namesVariable.ACCESSTOKEN,modelResult!!.token_type+" "+modelResult!!.access_token, applicationContext)
//                refreshToken(modelResult!!.user.Customer_ID,modelResult!!.token_type+" "+modelResult!!.access_token)
//            }
//            else{
//                toast("User Cannot Login.")
//            }
//
//        }

        //Using Retrofit
        var name=""
        try {
            CoroutineScope(Dispatchers.IO).launch {
//                val map = HashMap<String, RequestBody>()
//                map["Phone_Number"] = username.toRequestBody("multipart/form-data")
//                map["Password"] = password.toRequestBody("multipart/form-data")

                var loginreq : loginRequest = loginRequest()
                loginreq.Password = password
                loginreq.Phone_Number = username

                val service = APIClient.makeRetrofitService(applicationContext)
                val response = service.getLogin(loginreq)
                try {
                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            response.body()?.let {
                                val result = response.body()!!.access_token
                                logd(response.body().toString())
                                if (response.body()!!.Type != null&& (result == null || result.isEmpty())) {
                                    toast(response.body()!!.Msg)
                                } else if (!response.body()!!.user.IsDeleted) {
//                                    val modelResult: loginModel = gson.fromJson(result, loginModel::class.java)
                                    setStringSharedPref(namesVariable.EMAIL,response.body()!!.user.Email.toString(), applicationContext)
                                    setStringSharedPref(namesVariable.MOBILE,response.body()!!.user.Phone_Number.toString(), applicationContext)
                                    setStringSharedPref(namesVariable.PASSWORD,password, applicationContext)
                                    setStringSharedPref(namesVariable.FULLNAME,response.body()!!.user.Customer_Name.toString(), applicationContext)
                                    setStringSharedPref(namesVariable.CUSTOMERPOINTS,response.body()!!.user.Customer_Points.toString(), applicationContext)
                                    setStringSharedPref(namesVariable.REFERAL,response.body()!!.user.Referral_code.toString(), applicationContext)
                                    setStringSharedPref(namesVariable.USERID,response.body()!!.user.Customer_ID.toString(), applicationContext)
                                    setStringSharedPref(namesVariable.ACCESSTOKEN,response.body()!!.token_type+" "+response.body()!!.access_token, applicationContext)
                                    name=response.body()!!.user.Customer_Name
                                    toast("Welcome "+name)

//                                    refreshToken(response.body()!!.user.Customer_ID,response.body()!!.token_type+" "+response.body()!!.access_token)


                                    startActivity(Intent(applicationContext, SavedAddress::class.java))
                                }
                                else{
                                    toast("User Cannot Login.")
                                }
                            }
                        } else {
                            val gson = Gson()
                            val modelResult: loginError = gson.fromJson(response.errorBody()!!.charStream(), loginError::class.java)
                            toast(modelResult.Msg)
                        }
                    }
                } catch (e: HttpException) {
                    Log.e("REQUEST", "Exception ${e.message}")
                } catch (e: Throwable) {
                    Log.e("REQUEST", "Ooops: Something else went wrong")
                }
            }
        } catch (e: Throwable) {

        }
    }

    private fun refreshToken(customerID: String, token: String) {
        val client = OkHttpClient().newBuilder().build()
        val request = Request.Builder()
            .url("http://3.135.185.114/goancart-admin-api/index.php/api/customer/account/refreshToken?Customer_ID="+customerID)
            .method("GET", null)
            .addHeader("Authorization", token)
            .build()
        val response = client.newCall(request).execute()
        val res = response.body!!.string()
        val gson = Gson()
        val modelResult = gson.fromJson(res, loginModel::class.java)
        setStringSharedPref(namesVariable.ACCESSTOKEN,modelResult.token_type+" "+modelResult.access_token, applicationContext)
        startActivity(Intent(applicationContext, SavedAddress::class.java))


//        CoroutineScope(Dispatchers.IO).launch {
//            val service = APIClient.makeRetrofitService(applicationContext)
//            val response = service.refreshToken(customerID,token)
//            try {
//                withContext(Dispatchers.Main) {
//                    if (response.isSuccessful) {
//                        response.body()?.let {
//
////                            setStringSharedPref(namesVariable.USERID,response.body()!!.user.Customer_ID.toString(), applicationContext)
//                            setStringSharedPref(namesVariable.ACCESSTOKEN,response.body()!!.token_type+" "+response.body()!!.access_token, applicationContext)
//                            startActivity(Intent(applicationContext, MainActivity::class.java))
//                        }
//                    } else {
//                        val gson = Gson()
//                        val modelResult: loginError = gson.fromJson(response.errorBody()!!.charStream(), loginError::class.java)
//                        toast(modelResult.Msg)
//                    }
//                }
//            } catch (e: HttpException) {
//                Log.e("REQUEST", "Exception ${e.message}")
//            } catch (e: Throwable) {
//                Log.e("REQUEST", "Ooops: Something else went wrong")
//            }
//        }
    }

}
